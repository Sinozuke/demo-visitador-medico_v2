import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";

import Swiper from 'react-native-swiper';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.home_image}>
          <Swiper style={styles.wrapper} showsButtons={true} autoplay={true}>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.image} source={{uri:'https://corporacionphara.com/wp-content/uploads/2015/07/PHARA-MAMA.jpg'}} />
            </View>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.image} source={{uri:'https://corporacionphara.com/wp-content/uploads/2015/07/INMUNOPHARA.jpg'}} />
            </View>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.image} source={{uri:'https://corporacionphara.com/wp-content/uploads/2015/07/MUCO-FLASHjpg.jpg'}} />
            </View>
          </Swiper>
        </View>
        <View style={styles.menu_ops}>
          <View style={styles.menu_ops_row}>
            <TouchableOpacity style={styles.menu_op} onPress={() => this.props.navigation.push('Account')}>
              <ImageBackground
                style={styles.menu_img}
                resizeMode='cover'
                source={require('../../assets/images/menus/menu1.jpg')}
              >
              </ImageBackground >
              <View style={styles.menu_text}>
                <Text>News</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menu_op} onPress={() =>this.props.navigation.push('Category')}>
              <ImageBackground
                style={styles.menu_img}
                resizeMode='cover'
                source={require('../../assets/images/menus/menu2.jpg')}
              >
              </ImageBackground>
              <View style={styles.menu_text}>
                <Text>Category</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.menu_ops_row}>
            <TouchableOpacity style={styles.menu_op} onPress={() =>this.props.navigation.push('Shippings')}>
              <ImageBackground
                style={styles.menu_img}
                resizeMode='cover'
                source={require('../../assets/images/menus/menu3.jpg')}
              >
              </ImageBackground >
              <View style={styles.menu_text}>
                <Text>Shippings</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menu_op} onPress={() =>this.props.navigation.push('Account')}>
              <View style={{flex:4,backgroundColor:'powderblue'}}>
                <View style={styles.menu_icon}>
                  <Icon
                    name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
                    size={75}
                  />
                </View>
              </View >
              <View style={styles.menu_text}>
                <Text>Account</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:30,
    backgroundColor: '#fff',
  },
  home_image:{
    flex:2,
    margin: 8,
  },
  home_swiper:{
    flex:2,
    margin: 8,
  },
  image: {
    width:undefined,
    flex: 1
  },
  contentContainer: {
    paddingTop: 30,
  },
  wrapper: {
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  menu_ops:{
    flex: 4,
    marginHorizontal:3,
    marginBottom:5,
  },
  menu_ops_row:{
    flex: 1,
    flexDirection: 'row',
  },
  menu_op:{
    flex:1,
    backgroundColor: '#E6E6E6',
    margin:5,
    borderRadius:15,
    overflow:'hidden',
  },
  menu_icon:{
    flex: 1,
    alignSelf:'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  menu_img:{
    flex: 4,
  },
  menu_text:{
    flex:1,
    alignSelf:'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
});
