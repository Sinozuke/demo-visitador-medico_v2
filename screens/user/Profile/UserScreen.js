import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import { ExpoConfigView } from '@expo/samples';

import { CButton } from '../../../components/Buttons';

export default class UserScreen extends React.Component {

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <View style={{flex:1}}>
        <View style={{flex:10}} >
          <ExpoConfigView/>
        </View>
        <View style={styles.submit}>
          <CButton onPress={this._signOutAsync} text="Log Out" type="logout"/>
        </View>
      </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'stretch',
    justifyContent:'center',
    backgroundColor: 'skyblue',
    flexDirection: 'column',
  },
  input:{
    height:40,
    marginHorizontal:5,
    marginBottom:10,
    backgroundColor:'#F2F2F2',
  },
  contentContainer: {
    paddingTop: 30,
  },
  submit:{
    flex:1,
    margin:10,
  },
});
