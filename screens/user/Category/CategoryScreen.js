import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';

import Icon from "react-native-vector-icons/Ionicons";

export default class CategoryScreen extends React.Component {

  state = {
    categories: [
        {
            "name": "Respiratorios"
        },
        {
            "name": "Antibioticos"
        },
        {
            "name": "Gastricos"
        },
        {
            "name": "Miscelaneos"
        },
        {
            "name": "Anies"
        },
        {
            "name": "OTC"
        }
    ]
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.categories}
          showsVerticalScrollIndicator={false}
          style={styles.flatlist}
          renderItem={({item}) =>
            <TouchableOpacity
              style={styles.flatview}
              onPress={
                () =>this.props.navigation.push('CategoryList',{name:item.name,})
              }
            >
                <Image
                  style={{width:75, backgroundColor:'#92CBFF',}}
                  source={{uri:'https://corporacionphara.com/wp-content/uploads/2015/05/phara_logo.png'}}
                />
                <View style={{flex: 1, backgroundColor: '#E6E6E6',flexDirection:'row'}}>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={2}
                      style={styles.name}
                    >
                      {item.name}
                    </Text>
                  </View>
            </TouchableOpacity>
          }
          keyExtractor={item => item.name}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatlist:{
    marginTop:5,
  },
  menu_icon:{
    flex: 1,
    alignSelf:'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  flatview: {
    flex:1,
    height:75,
    marginHorizontal:15,
    marginVertical:5,
    borderRadius:15,
    justifyContent: 'center',
    flexDirection:'row',
    overflow:'hidden',
  },
  name: {
    flex:1,
    fontSize: 18,
    fontFamily: 'Verdana',
    alignSelf:'center',
    paddingLeft:4,
  },
  cat_op:{
    flexDirection:'row',
    backgroundColor: 'green',
    borderRadius:5,
  },
});
