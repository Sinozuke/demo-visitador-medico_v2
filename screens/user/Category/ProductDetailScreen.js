import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { CButton } from '../../../components/Buttons';

export default class ProductDetailScreen extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        name    : "",
        img_url : "",
      }
    }

    static navigationOptions = ({ navigation }) => ({
     title: `${navigation.state.params.name}`,
      headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
         headerStyle:{
             backgroundColor:'white',
         },
     });

    render() {

      const { navigation } = this.props;
      const name = navigation.getParam('name', 'Dummy-Name');
      const description = navigation.getParam('desc', 'Dummy-Text');
      const img_url = navigation.getParam('img_url', 'http://drhealthnut.com/wp-content/uploads/2013/11/dummy-image-landscape-1024x585.jpg');

      this.state.name=name;
      this.state.img_url=img_url;

      return (
        <View style={styles.container}>
          <ScrollView style={styles.scroll}>
            <Image
              style={styles.img}
              source={{uri: img_url}}
            />
            <View style={styles.product_info}>
              <Text style={styles.texto}>{description}</Text>
            </View>
          </ScrollView>
          <View style={styles.submit}>
            <CButton onPress={this._makeOrder} text="Make an Order" type="default"/>
          </View>
        </View>
      );
    }

    _makeOrder = () => this.props.navigation.navigate('Order',{
                            name:this.state.name,
                            img_url:this.state.img_url,
                          })

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:10,
  },
  scroll:{
    flex: 1,
  },
  img:{
    height:250,
  },
  product_info:{
    marginVertical:20,
  },
  texto:{
    textAlign:'justify',
  },
  submit:{
    marginHorizontal:10,
    height:30,
  },
});
