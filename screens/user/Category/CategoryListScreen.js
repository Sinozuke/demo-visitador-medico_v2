import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';

import Icon from "react-native-vector-icons/Ionicons";

export default class CategoryListScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
   title: `${navigation.state.params.name}`,
    headerTitleStyle : {textAlign: 'center',alignSelf:'center'},
       headerStyle:{
           backgroundColor:'white',
       },
   });

  state = {
    cat_types:[],
    respiratorios: [
        {
            "name"    : "MucoFlash",
            "desc"    : "Bronquitis agudas y crónicas; bronquitis asmatiforme, asma y en general afecciones pulmonares agudas y crónicas que cursen con retención de secreciones y broncoespasmo.",
            "img_url" : "https://corporacionphara.com/wp-content/uploads/2015/05/muco_flash-480x425.jpg"
        },
        {
            "name"    : "InmunoPhara",
            "desc"    : "InmunoPhara® – Q3 en todo proceso de las vías respiratorias tanto altas como bajas; en procesos que inician con síntomas leves como también crónicos y agudos: bronquitis, sinusitis, amigdalitis y rinofaringitis. IMPORTANTE en la prevención de enfermedades virales, bacterianas aumentando el sistema inmunológico respiratorio.",
            "img_url" : "https://corporacionphara.com/wp-content/uploads/2015/05/inmuno_phara-480x425.jpg",
        },
        {
            "name": "PharaMucol",
            "desc"    : "PHARA MUCOL® Está indicado en cualquier proceso agudo o crónico que requiera de la fluidificación de las secreciones respiratorias: bronquitis aguda y crónica, bronquitis asmatiforme, asma bronquial, bronconeumonía, neumonía, laringitis, traqueítis, rinofaringitis, sinusitis, fibrosis quística y bronquiectasias.",
            "img_url" : "https://corporacionphara.com/wp-content/uploads/2015/07/phmuco-480x425.jpg",
        },
        {
            "name"    : "Phara Mucol LT",
            "desc"    : "Phara Mucol – LT® está indicado para el alivio sintomático de los procesos alérgicos respiratorios asociados con tos no productiva y presencia de moco en el tracto respiratorio. También están indicados para el tratamiento de pacientes con procesos broncopulmonares alérgicos que cursen con aumento de la viscosidad y adherencia del moco, en los que es necesario mantener libre de secreciones las vías respiratorias. Sus principales indicaciones son: rinitis alérgica asociada con tos, bronquitis, bronquiectasias, sinusitis, neumonía, bronconeumonía, atelectasia por tapón mucoso, traqueotomía, así como agente profiláctico pre y postquirúrgico, especialmente en ancianos en quienes se sospecha alguna condición alérgica.",
            "img_url" : "https://corporacionphara.com/wp-content/uploads/2015/05/phmucol-480x425.jpg",
        }
    ],
    antibioticos:[
      {
            "name"    : "Phara Cef",
            "desc"    : "Phara Cef® es un antibiótico de cefalosporina, está indicado en el tratamiento de las siguientes infecciones cuando son causadas por microorganismos susceptibles: Infecciones del tracto respiratorio superior e inferior. Infecciones de la piel y tejido blando. Infecciones del tracto genitourinario. Otras infecciones: osteomielitis y artritis séptica.",
            "img_url" : "https://corporacionphara.com/wp-content/uploads/2015/06/phara_cef-480x425.jpg"
      },
      {
            "name": "Phara Clor"
      },
      {
            "name": "Fixibac"
      },
      {
            "name": "Phara Trax"
      },
      {
            "name": "Phara Ciprox"
      },
      {
            "name": "Phara Flox"
      },
      {
            "name": "Phara Clary"
      },
      {
            "name": "Phara Sulf"
      },
    ],
    gastricos:[
      {
            "name": "Phara Lanz"
      },
      {
            "name": "Phara Pranex"
      },
      {
            "name": "Phara Pramid"
      },
      {
            "name": "Phara Ameb"
      },
      {
            "name": "Phara Gel"
      },
      {
            "name": "PRANEX HP"
      },
    ],
    miscelaneos:[
      {
            "name": "Phara Benk"
      },
      {
            "name": "Phara Pril"
      },
      {
            "name": "Setrasec"
      },
      {
            "name": "Phara Ferro"
      },
      {
            "name": "Phara Mama"
      },
      {
            "name": "Phara Prenat"
      },
    ],
    anies:[
      {
            "name": "Airflac"
      },
      {
            "name": "Dolo Pharma Nervo"
      },
      {
            "name": "Exken Plus"
      },
      {
            "name": "Phara Xib"
      },
      {
            "name": "Phara Profen"
      },
      {
            "name": "Phara Fenac"
      },
    ],
    otc:[
        {
              "name": "Phara Grip"
        },
        {
              "name": "Phara Nervo"
        },
        {
              "name": "Foly Tonic"
        },
        {
              "name": "Phara Benzol"
        },
    ],
  }

  render_category(name){
    switch (name) {
      case "Respiratorios":
        this.state.cat_types = this.state.respiratorios;
      break;
      case "Antibioticos":
        this.state.cat_types = this.state.antibioticos;
      break;
      case "Gastricos":
        this.state.cat_types = this.state.gastricos;
      break;
      case "Miscelaneos":
        this.state.cat_types = this.state.miscelaneos;
      break;
      case "Anies":
        this.state.cat_types = this.state.anies;
      break;
      case "OTC":
        this.state.cat_types = this.state.otc;
      break;
    }
  }


  render() {
    const { navigation } = this.props;
    const cat_name = navigation.getParam('name', 'NO-NAME');
    this.render_category(cat_name);
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.cat_types}
          showsVerticalScrollIndicator={false}
          style={styles.flatlist}
          renderItem={({item}) =>
            <TouchableOpacity
              style={styles.flatview}
              onPress={
                () =>this.props.navigation.push('ProductDetail',{
                  name:item.name,
                  desc:item.desc,
                  img_url:item.img_url,
                })
              }
            >
                <Image
                  style={{width:75}}
                  source={{uri:item.img_url}}
                />
                <View style={{flex: 1, backgroundColor: '#E6E6E6',flexDirection:'row'}}>
                    <Text
                      adjustsFontSizeToFit
                      numberOfLines={2}
                      style={styles.name}
                    >
                      {item.name}
                    </Text>
                  </View>
            </TouchableOpacity>
          }
          keyExtractor={item => item.name}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatlist:{
    marginTop:5,
  },
  menu_icon:{
    flex: 1,
    alignSelf:'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  flatview: {
    flex:1,
    height:75,
    marginHorizontal:15,
    marginVertical:5,
    borderRadius:10,
    justifyContent: 'center',
    flexDirection:'row',
    overflow:'hidden',
  },
  name: {
    flex:1,
    fontSize: 18,
    fontFamily: 'Verdana',
    alignSelf:'center',
    paddingLeft:4,
  },
  cat_op:{
    flexDirection:'row',
    borderRadius:5,
  },
});
