import React from 'react';
import {
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

import Globals from '../../constants/Colors';

import { CButton } from '../../components/Buttons';

export default class LoginScreen extends React.Component {

  render() {
    return (

      <View style={styles.container}>
        <Image
          style={styles.login_img}
          source={{uri:'https://corporacionphara.com/wp-content/uploads/2015/05/phara_logo.png'}}
        />
        <TextInput
          style={styles.input}
          placeholder="  email@example.com"
          keyboardType='email-address'
        />
        <TextInput
          style={styles.input}
          placeholder="  Password"
          secureTextEntry={true}
          clearTextOnFocus={true}
          returnKeyType="go"
        />
        <View style={styles.submit}>
          <CButton onPress={this._signInAsync} text="Login" type="default"/>
        </View>
      </View>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'stretch',
    justifyContent:'center',
    backgroundColor: Globals.colors.primary,
    flexDirection: 'column',
  },
  input:{
    height:40,
    marginHorizontal:25,
    marginBottom:10,
    backgroundColor:'#fff',
    borderRadius:20,
  },
  contentContainer: {
    paddingTop: 30,
  },
  submit:{
    marginHorizontal:25,
    flexDirection:'row',
    height:35,
  },
  login_img:{
      height:125,
      width:125,
      alignSelf:'center',
      marginBottom:20,
  },
});
