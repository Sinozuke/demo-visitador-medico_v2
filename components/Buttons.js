import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import Globals from '../constants/Colors';

export class CButton extends React.Component {
  render() {
    return(
      <TouchableOpacity
        style={[this.buttomtype(this.props.type),styles.submitb]}
        onPress={this.props.onPress}>
          <View style={styles.submitText}>
            <Text style={styles.txt}>{this.props.text}</Text>
          </View>
      </TouchableOpacity>
    );
  }

  buttomtype = function(btype){
    switch (btype) {
      case "logout"://Logout Type
        return{
          backgroundColor:Globals.colors.logout_button,
        }
      default:
        return{
          backgroundColor:Globals.colors.default_button,
        }
    }
  }

}


const styles = StyleSheet.create({
  submitb:{
    flex:1,
    borderRadius:20,
    borderColor: '#fff',
  },
  submitText:{
      flex:1,
      alignSelf:'center',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
  },
  txt:{
    color:'#fff',
    fontWeight:'bold',
  },
});
