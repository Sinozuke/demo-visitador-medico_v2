const tintColor = '#2f95dc';

export default {
  tintColor,
  colors:{
      primary         : '#E6E6E6',
      default_button  : '#68a0cf',
      logout_button   : '#FE2E2E',
  },
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
