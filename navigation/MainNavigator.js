import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from '../screens/user/HomeScreen';
import ShippingsScreen from '../screens/user/Shippings/ShippingsScreen';
import OrderScreen from '../screens/user/Shippings/OrderScreen';
import UserScreen from '../screens/user/Profile/UserScreen';
import CategoryScreen from '../screens/user/Category/CategoryScreen';
import CategoryListScreen from '../screens/user/Category/CategoryListScreen';
import ProductDetailScreen from '../screens/user/Category/ProductDetailScreen';

export default createStackNavigator({
  Home:{
    screen:HomeScreen,
  },
  Category:{
    screen: CategoryScreen,
    navigationOptions:{
      title:'Category',
    },
  },
  CategoryList:{
    screen: CategoryListScreen,
  },
  ProductDetail:{
    screen: ProductDetailScreen,
  },
  Shippings:{
    screen: ShippingsScreen,
    navigationOptions:{
      title:'Shippings',
    },
  },
  Order:{
    screen: OrderScreen,
    navigationOptions:{
      title:'Make an Order',
    },
  },
  Account:{
    screen: UserScreen,
    navigationOptions:{
      title:'Account',
    },
  },
},{
  initialRouteName:'Home',
});
