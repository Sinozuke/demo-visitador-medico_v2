import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import LoginScreen from '../screens/auth/LoginScreen';
import SignUpScreen from '../screens/auth/SignUpScreen';
import ForgotPasswordScreen from '../screens/auth/ForgotPasswordScreen';

export default createStackNavigator({
  Login: {
    screen:LoginScreen,
  },
  SignUp:{
    screen: SignUpScreen,
  },
  ForgotPassword:{
    screen: ForgotPasswordScreen,
  }
},{
  defaultNavigationOptions:{
    tabBarVisible:false,
    header:null,
  },
  initialRouteName:'Login',
});
