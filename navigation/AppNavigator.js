import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainStack from './MainNavigator';
import AuthStack from './AuthNavigator';
import AuthLoadingScreen from '../screens/auth/AuthLoadingScreen';

export default createAppContainer(createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  App : MainStack,
  Auth: AuthStack,
},{
  initialRouteName:'AuthLoading',
}
));
